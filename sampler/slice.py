#!/usr/bin/env python
import numpy as np

def conditionalslice(loglikelihood,S,z0,mode,m,p,w,*,adapt=0,ax=[]):
    
    xPlot = ax!=[]
    
    if xPlot:
        ax.plot(z0[0],z0[1],'bo')
        pass
    
    J = len(z0)
    LL0 = loglikelihood(z0)
    Z_sample = np.ones([S+1,J])*np.nan
    Z_sample[0,:] = z0  
    
    
    V = 5*np.eye(J)
    for s in range(S):
        
        for j in range(J):
            v = V[:,j]
        
            g = lambda t : loglikelihood(v*t+z0)
            t0 = 0
            [t1,LL1] = fOneSliceSample(mode,g,t0,LL0,w,m,p) 
            z1 = v*t1+z0
            
            if xPlot :
                if s>=adapt*2:
                    ax.plot(np.array([z0[0],z1[0]]),np.array([z0[1],z1[1]]),'k-')
                    pass
                else:
                    ax.plot(np.array([z0[0],z1[0]]),np.array([z0[1],z1[1]]),'-',color='lightgrey')
                    pass
            z0 = z1
            LL0 = LL1 
            pass
        
        Z_sample[s+1,:] = z0
        
        if adapt>0 and s>adapt:
            if np.mod(s,adapt)==0 and (s>J and s<np.inf):
                Theta = np.array(Z_sample[:s,:])
                if xPlot:
                    ax.plot(Theta[:s,0],Theta[:s,1],'.',color='lightgrey')
                    pass
                XX = Theta[adapt:,:]
                mn = np.mean(XX,axis=0)
                [UU,eigenval,Vt]=np.linalg.svd(XX-mn,full_matrices=False)
                lambdas = eigenval/np.sqrt(s)
                V = 5*np.dot(np.diag(lambdas),Vt).T
                xSVD = True
                pass
        pass
    
    if xPlot:
        Theta = np.array(Z_sample)
        ax.plot(Theta[1:,0],Theta[1:,1],'r.')
        pass
    
    return Z_sample


def hitandrunslice(loglikelihood,S,z0,mode,m,p,w,*,adapt=0,ax=[]):
    
    xPlot = ax!=[]
    
    if xPlot:
        ax.plot(z0[0],z0[1],'bo')
        pass
    
    J = len(z0)
    LL0 = loglikelihood(z0)
    Z_sample = np.ones([S+1,J])*np.nan
    Z_sample[0,:] = z0
    
    
    P = 5*np.eye(J)
    for s in range(S):
        
        if True:
            u = np.random.randn(J)
            u = u/np.linalg.norm(u)
            v = np.dot(P,u)
        
            g = lambda t : loglikelihood(v*t+z0)
            t0 = 0
            [t1,LL1] = fOneSliceSample(mode,g,t0,LL0,w,m,p) 
            z1 = v*t1+z0
            
            if xPlot :
                if s>=adapt*2:
                    ax.plot(np.array([z0[0],z1[0]]),np.array([z0[1],z1[1]]),'k-')
                    pass
                else:
                    ax.plot(np.array([z0[0],z1[0]]),np.array([z0[1],z1[1]]),'-',color='lightgrey')
                    pass
            z0 = z1
            LL0 = LL1 
            pass
        
        Z_sample[s+1,:] = z0
        
        if adapt>0 and s>adapt:
            if np.mod(s,adapt)==0 and (s>J and s<np.inf):
                Theta = np.array(Z_sample[:s,:])
                if xPlot:
                    ax.plot(Theta[:s,0],Theta[:s,1],'.',color='lightgrey')
                    pass
                XX = Theta[adapt:,:]
                mn = np.mean(XX,axis=0)
                [UU,eigenval,Vt]=np.linalg.svd(XX-mn,full_matrices=False)
                lambdas = eigenval/np.sqrt(s)
                P = 5*np.dot(np.diag(lambdas),Vt).T
                xSVD = True
                pass
        pass
    
    if xPlot:
        Theta = np.array(Z_sample)
        ax.plot(Theta[1:,0],Theta[1:,1],'r.')
        pass
    
    return Z_sample

def fOneSliceSample(mode,f,x0,f0,w,m,p): 
    alpha  = np.random.rand(1) 
    y =np.log(alpha)+f0   
    
    if mode=='stepout':
        [L,R] = fStepOut(f,x0,y,w,m) ;
        pass
    elif mode=='double':
        [L,R] = fDouble(f,x0,y,w,p) ;
        pass 
    elif mode=='fixed':
        U = np.random.rand(1);
        L = x0-w*U ;
        R = L + w ;
        pass
    
    Lbar = L ;
    Rbar = R ;
    xAccept = False ;
    #iter = 0 ;
    while not(xAccept):
        #iter = iter +1;
        U = np.random.rand(1);
        x1 = Lbar + U *(Rbar - Lbar );
        f1 = f(x1) ;
        if y<f1 and fAccept(mode,f,x0,x1,y,w,L,R):
            xAccept = True
            pass
        if not(xAccept):
            if x1<x0:
                Lbar = x1
                pass
            else:
                Rbar = x1
                pass
            pass
        pass
    
    return x1,f1

def fAccept(mode,f,x0,x1,y,w,L,R):
    xAccept = True ;
    if mode=='double':
        Lhat = L ;
        Rhat = R ;
        fLhat = np.nan ; 
        fRhat = np.nan ; 
        D = False ;
        while Rhat-Lhat > 1.1 * w:
            M = (Lhat+Rhat)/2 ;
            if (x0<M and x1>=M) or (x0>=M and x1<M):
                D = True ;
                pass
            if x1<M:
                Rhat = M ;
                fRhat = f(Rhat) ;
                pass
            else:
                Lhat = M  ;
                fLhat = f(Lhat) ;
                pass
            if D:
                if np.isnan(fLhat):
                    fLhat = f(Lhat) ;
                    pass
                if np.isnan(fRhat):
                    fRhat = f(Rhat) ;
                    pass
                if y>=fLhat and y>=fRhat:
                    xAccept = False;
                    pass
                pass
            pass
        pass
    return xAccept

def fDouble(f,x0,y,w,p):
    
    U = np.random.rand(1);
    L = x0-w*U ;
    R = L + w ;
    K = p ;
    fL = f(L) ;
    fR = f(R) ;
    while K>0 and (y<fL or y<fR):
        V = np.random.rand(1)*2;
        if V<1:
            L = L -(R-L) ;
            fL = f(L) ;
            pass
        else:
            R = R +(R-L) ;
            fR = f(R) ;
            pass
        K = K-1 ;
        pass
    
    return L,R

def fStepOut(f,x0,y,w,m):
    U = np.random.rand(1);
    L = x0-w*U ;
    R = L + w ;
    V = np.random.rand(1);
    J = floor(m*V);
    K = (m-1)-J ;
    while J>0 and y<f(L):
        L = L-w ;
        J = J-1 ;
        pass
    
    while K>0 and y<f(R):
        R = R+w ;
        K = K-1 ;
        pass
    
    return L,R