#!/usr/bin/env python
import numpy as np

def metropolis(loglikelihood,S,z0,sigma_prop,*,ax=[]):
    
    xPlot = ax!=[]
    
    if xPlot:
        ax.plot(z0[0],z0[1],'bo')
        pass
    
    J = len(z0)
    LL0 = loglikelihood(z0)
    Z_sample = np.ones([S+1,J])*np.nan
    Z_sample[0,:] = z0
    
    for s in range(S):
        z1 = z0+np.random.randn(J)*sigma_prop
        LL1 = loglikelihood(z1)
        if LL1>LL0:
            xAccept = True;
            pass
        else:
            U = np.random.rand(1)
            logU = np.log(U)
            if LL1-LL0>logU:
                xAccept = True
                pass
            else:
                xAccept = False
                pass
            pass
        if xAccept:
            if xPlot:
                ax.plot(np.array([z0[0],z1[0]]),np.array([z0[1],z1[1]]),'k-')
                pass
            z0 = z1
            LL0 = LL1
            pass
        Z_sample[s+1,:] = z0
        pass
    
    if xPlot:
        Theta = np.array(Z_sample)
        ax.plot(Theta[1:,0],Theta[1:,1],'r.')
        pass
    
    return Z_sample
