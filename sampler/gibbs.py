#!/usr/bin/env python
from scipy.stats import chi2
import numpy as np

def univariatenormal(x,S,*,ax=[]):
    
    xPlot = ax!=[]
    
    J =2
    N = len(x)
    m = np.sum(x)/N
    
    mu = [0]
    gamma = 0
    Z_sample = np.ones([S+1,J])*np.nan
    Z_sample[0,0] = mu[0]
    Z_sample[0,1] = gamma
    
    if xPlot:
        ax.plot(mu,gamma,'bo')
        pass
    
    for s in range(S):
        
        if xPlot:
            mu_old = mu*1
            gamma_old = gamma*1
            pass
        
        # Conditional mean
        sd_n0 = np.sqrt(np.exp(gamma)/N )
        mu = np.random.randn(1)*sd_n0+m
        
        if xPlot:
            ax.plot(np.array([mu_old,mu]),np.array([gamma_old,gamma_old]),'k-')
            mu_old = mu
            pass
        
        # Conditional variance
        chi2sample = chi2.rvs(N, size=1)
        sse = np.sum((x-mu)**2)
        s2 = sse/N
        logs2 = np.log(s2)
        gamma=np.log(N)+logs2-np.log(chi2sample)[0]
        Z_sample[s+1,0] = mu[0]
        Z_sample[s+1,1] = gamma
        
        if xPlot:
            ax.plot(np.array([mu,mu]),np.array([gamma_old,gamma]),'k-')
            gamma_old = gamma
            pass
        pass
    
    if xPlot:
        Theta = np.array(Z_sample)
        ax.plot(Theta[1:,0],Theta[1:,1],'r.')
        pass
    
    return Z_sample